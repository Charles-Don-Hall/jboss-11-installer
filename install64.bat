@time /t 
@echo Removing old 1.8 JDKs
msiexec /x{32A3A4F4-B792-11D6-A78A-00B0D0180151} /passive
msiexec /x{26A24AE4-039D-4CA4-87B4-2F64180151F0} /passive
msiexec /x{26A24AE4-039D-4CA4-87B4-2F86416045FF} /passive
@echo Installing 64-bit JDK...
jdk-8u151-windows-x64.exe /s /L %~dp0\jdk.log 
@echo Removing old Developer Studio
rmdir /s /q "c:\Program Files\JBoss"
@echo Installing Developer Studio
java -jar devstudio-11.1.0.GA-installer-standalone.jar InstallConfigRecord64.xml >jboss.log 2>&1
@time /t